# Eclipse
.classpath
.project
.settings/
 
# Maven
log/
target/
dist/

# Applications
*.app
*.exe
*.war

# Log files
*.log

# Package files
*.jar

# Compiled Java class files
*.class