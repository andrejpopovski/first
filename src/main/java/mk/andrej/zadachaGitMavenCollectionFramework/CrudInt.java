package mk.andrej.zadachaGitMavenCollectionFramework;

public interface CrudInt {
	public void add(Student student);

	public void update(String id, String key, String value);

	public void delete(String id);

	public void read();

}
