package mk.andrej.zadachaGitMavenCollectionFramework;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Student {
	private String id;
	private String firstName;
	private String lastName;

}
