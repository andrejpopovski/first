package mk.andrej.zadachaGitMavenCollectionFramework;

public class App {

	public static void main(String[] args) {
		Crud c = new Crud();
		c.add(new Student("1", "Andrej", "Popovski"));
		c.add(new Student("2", "Nikola", "Nikolovski"));
		c.add(new Student("3", "Kire", "Kirev"));
		c.read();

		System.out.println("\nUpdating id 2, changing the first name\n");
		c.update("2", "firstname", "Stavre");
		c.read();

		System.out.println("\nDeleting id 1 and 2\n");
		c.delete("1");
		c.delete("2");
		
		c.read();

	}

}
