package mk.andrej.zadachaGitMavenCollectionFramework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Crud implements CrudInt {

	private List<Map<String, String>> maplist = new ArrayList<>();

	Map<String, String> studentMap;

	@Override
	public void add(Student student) {
		studentMap = new HashMap<String, String>();

		studentMap.put("id", student.getId());
		studentMap.put("firstname", student.getFirstName());
		studentMap.put("lastname", student.getLastName());
		maplist.add(studentMap);
	}

	@Override
	public void update(String id, String key, String value) {

		for (Map<String, String> map : maplist) {
			if (map.containsValue(id)) {
				map.replace(key, value);
			}
		}
	}

	@Override
	public void delete(String id) {
		for (Map<String, String> map : maplist) {
			if (map.containsValue(id)) {
				maplist.remove(map);
				break;
			}
		}
	}

	@Override
	public void read() {
		System.out.println(maplist);
	}

}
